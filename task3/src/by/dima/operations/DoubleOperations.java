package by.dima.operations;

/**
 * Defines Operations with Double type.
 * @see java.lang.Double
 */
public class DoubleOperations implements Operatable<Double> {
    public Double add(Double a, Double b) {
        return a + b;
    }
    public Double sub(Double a, Double b) {
        return a - b;
    }
    public Double mul(Double a, Double b) {
        return a * b;
    }
    public Double div(Double a, Double b) {
        return a / b;
    }
}
