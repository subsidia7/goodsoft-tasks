package by.dima.operations;

/**
 * Defines Operations with String type.
 * @see java.lang.String
 */
public class StringOperations implements Operatable<String> {
    /**
     * Concatenate strings, adding b to the end of a.
     * @return Concatenated String.
     */
    public String add(String a, String b) {
        return a.concat(b);
    }
    /**
     * Replace b string from a string.
     * @return New string a without b.
     */
    public String sub(String a, String b) {
        if (!a.contains(b)) {
            return a;
        }
        return a.replace(b, "");
    }
    /**
     * Makes characters multiplication.
     * @return If a is "xy" and b is "abc" returns "xaxbxcyaybyc".
     */
    public String mul(String a, String b) {
        int cSize = a.length() * b.length() * 2;
        char[] c = new char[cSize];
        for (int i = 0, k = 0; i < a.length(); ++i) {
            for (int j = 0; j < b.length(); j++, ++k) {
                c[k * b.length()] = a.charAt(i);
                c[k * b.length() + 1] = b.charAt(j);
            }
        }
        return new String(c);
    }
    /**
     * Cuts a string by dividing a length to b length.
     * @return If a is "abcd" and b is "ty" returns "ab".
     */
    public String div(String a, String b) {
        int retLen = a.length() / b.length();
        char[] ret = new char[retLen];
        a.getChars(0, retLen, ret, 0);
        return new String(ret);
    }
}
