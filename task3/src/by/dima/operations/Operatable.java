package by.dima.operations;

/**
 * Implement this interface to a Service-Class.
 * Contains methods declarations.
 * @param <T> is a type of class for operations.
 */
public interface Operatable<T> {
    /**
     * Performs addition operation.
     * @param a Left addition Param.
     * @param b Right addition Param.
     * @return Sum of type T.
     */
    T add(T a, T b);
    /**
     * Performs subtraction operation.
     * @param a Left subtraction Param.
     * @param b Right subtraction Param.
     * @return Sub of type T.
     */
    T sub(T a, T b);
    /**
     * Performs multiply operation.
     * @param a Left multiply Param.
     * @param b Right multiply Param.
     * @return Mul of type T.
     */
    T mul(T a, T b);
    /**
     * Performs dividing operation.
     * @param a Left dividing Param.
     * @param b Right dividing Param.
     * @return Div of type T.
     */
    T div(T a, T b);
}
