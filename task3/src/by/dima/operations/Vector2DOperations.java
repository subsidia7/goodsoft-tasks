package by.dima.operations;
import by.dima.types.Vector2D;
/**
 * Defines Operations with Vector2D.
 * @see by.dima.types.Vector2D
 */
public class Vector2DOperations implements Operatable<Vector2D> {
    public Vector2D add(Vector2D a, Vector2D b) {
        return new Vector2D(a.x + b.x, a.y + b.y);
    }
    public Vector2D sub(Vector2D a, Vector2D b) {
        return new Vector2D(a.x - b.x, a.y - b.y);
    }
    public Vector2D mul(Vector2D a, Vector2D b) {
        return new Vector2D(a.x * b.x, a.y * b.y);
    }
    public Vector2D div(Vector2D a, Vector2D b) {
        return new Vector2D(a.x / b.x, a.y / b.y);
    }
}
