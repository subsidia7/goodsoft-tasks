package by.dima.types;

/**
 * Class describes two-dimentional math Vector.
 * @see <a href="https://en.wikipedia.org/wiki/Euclidean_vector">Euclidean Vector</a>
 */
public class Vector2D {
    /**
     * Coordinate on X axis.
     */
    public int x;
    /**
     * Coordinate on Y axis.
     */
    public int y;
    /**
     * Vector constructor.
     * @param x Coordinate on X axis.
     * @param y Coordinate on Y axis.
     */
    public Vector2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Copy constructor.
     * @param a Vector2D variable should be copied.
     */
    public Vector2D(Vector2D a) {
        super();
        this.x = a.x;
        this.y = a.y;
    }

    /**
     * Compare Vectors for equality. Vectors equal if their coordinates equal.
     * @param o Comparable object
     * @return true if Vectors equal, instead false.
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Vector2D c)) {
            return false;
        }
        return c.x == this.x && c.y == this.y;
    }

    /**
     * Converting Vector2D to String type.
     * @return String of {x, y} pattern. For example, "{1,2}".
     */
    @Override
    public String toString() {
        return new String("{" + this.x + ", " + this.y + "}");
    }
}
