package by.dima;

import by.dima.operations.Operatable;
import by.dima.operations.IntegerOperations;
import by.dima.operations.DoubleOperations;
import by.dima.operations.StringOperations;
import by.dima.operations.Vector2DOperations;
import by.dima.types.Vector2D;

/**
 * Class with program entry point.
 */
public class Main {
    /**
     * Contains examples of using OperationsClasses.
     * @param args String array of command line arguments.
     */
    public static void main(String[] args) {
        Operatable<Integer> opI = new IntegerOperations();
        System.out.println(opI.add(1123, 44451));

        Operatable<Double> opD = new DoubleOperations();
        System.out.println(opD.sub(1.2, 2.5));

        Operatable<String> opS = new StringOperations();
        System.out.println(opS.mul("abck", "xy"));

        Operatable<Vector2D> opV2D = new Vector2DOperations();
        System.out.println(opV2D.div(new Vector2D(12, 8), new Vector2D(3,4)));
    }
}
