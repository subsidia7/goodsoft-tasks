package by.dima.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.Date;

public class TestServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException {
        process(req, res);
    }
    private static void process(HttpServletRequest req, HttpServletResponse res) {
        try (PrintWriter out = res.getWriter()){
            res.setContentType("text/html");
            HttpSession s = req.getSession(true);
            String counter = String.valueOf(increase(s));
            StringBuffer url = req.getRequestURL();
            out.write("My session counter: " + counter);
            out.write("<br>Session creation time: " + new Date(s.getCreationTime()));
            out.write("<br>Session last access time: " + new Date(s.getLastAccessedTime()));
            out.write("<br>URL: " + url);
            int timeLive = 20;
            s.setMaxInactiveInterval(timeLive);
            out.write("<br>Max session lifetime: " + timeLive + "sec");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed: " + e);
        }

    }
    private static int increase(HttpSession s) {
        Integer counter = (Integer)s.getAttribute("counter");
        if (counter == null) {
            s.setAttribute("counter", 1);
            return 1;
        }
        ++counter;
        s.setAttribute("counter", counter);
        return counter;
    }
}
