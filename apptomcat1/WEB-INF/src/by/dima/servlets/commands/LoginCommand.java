package by.dima.servlets.commands;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.dima.servlets.logic.LoginLogic;
import by.dima.servlets.manager.MessageManager;
import by.dima.servlets.manager.ConfigurationManager;

public class LoginCommand implements Command {
	private static final String PARAM_NAME_LOGIN = "login";
	private static final String PARAM_NAME_PASSWORD = "password";
	public String execute(HttpServletRequest request,
						  HttpServletResponse response) 
						  throws ServletException, IOException {
		String page = null;
		String login = request.getParameter(PARAM_NAME_LOGIN);
		String pass = request.getParameter(PARAM_NAME_PASSWORD);
		HttpSession s = request.getSession(true);
		int timeLive = 20;
		s.setMaxInactiveInterval(timeLive);
		if (LoginLogic.checkLogin(login, pass)) {
			s.setAttribute("isLogin", true);
			s.setAttribute("login", login);
			s.setAttribute("password", pass);
			request.setAttribute("user", login);
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE_PATH);
		} else {
			s.setAttribute("is_login", false);
			request.setAttribute("errorMessage", MessageManager.getInstance().getProperty(MessageManager.LOGIN_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}