package by.dima.filters;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import by.dima.servlets.manager.ConfigurationManager;
public class UnauthFilter implements Filter {
    private FilterConfig filterConfig;
    public void init(final FilterConfig cfg) {
        filterConfig = cfg;
    }
    public void doFilter(final ServletRequest req, final ServletResponse res, FilterChain filterChain)
    throws IOException, ServletException{
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        boolean loggedIn = session != null && session.getAttribute("isLogin") != null
                && (boolean)session.getAttribute("isLogin");
        if (loggedIn) {
            filterChain.doFilter(req, res);
        } else {
            RequestDispatcher rd = req.getRequestDispatcher(ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH));
            rd.forward(req, res);
        }
    }
    public void destroy() {
    }
}
