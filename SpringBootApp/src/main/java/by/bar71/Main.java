package by.bar71;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.mybatis.spring.annotation.MapperScan;

@Configuration
@ServletComponentScan
@SpringBootApplication
@MapperScan("by.bar71.data")
public class Main extends SpringBootServletInitializer{

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

}