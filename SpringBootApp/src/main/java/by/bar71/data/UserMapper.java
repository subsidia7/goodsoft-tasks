package by.bar71.data;

import by.bar71.domain.Role;
import by.bar71.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    User selectUserByLoginAndPassword(String login, String password);

    User selectUserByLogin(String login);

    List<Role> selectUserRoles(String login);

    List<User> selectUsers();

    List<Role> selectRoles();

    void insertUser(User user);

    void insertUserRoles(String login, String naming);

    void updateUser(User user);

    void deleteUserByLogin(String login);

    void deleteUserRoles(String login);

}
