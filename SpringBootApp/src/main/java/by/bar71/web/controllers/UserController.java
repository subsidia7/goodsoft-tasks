package by.bar71.web.controllers;

import by.bar71.domain.Role;
import by.bar71.domain.User;
import by.bar71.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController( UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public String users() {
        return "admin-commands/users";
    }

    @GetMapping("/addUser")
    public ModelAndView addUser() {
        ModelAndView mv = new ModelAndView("admin-commands/add-user");
        mv.getModelMap().addAttribute("user", new User());
        return mv;
    }

    @GetMapping("/editUser")
    public ModelAndView editUser(@RequestParam("editableLogin") String editableLogin) {
        ModelAndView mv = new ModelAndView("admin-commands/edit-user");
        User u = userService.findByLogin(editableLogin);
        mv.getModelMap().addAttribute("editableUser", userService.findByLogin(editableLogin));
        return mv;
    }

    @PostMapping("/addUser")
    public String addUser(@Validated @ModelAttribute("user") User user, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "admin-commands/add-user";
        }
        userService.addUser(user);
        model.addAttribute("dataBase", getDataBase());
        return "admin-commands/users";
    }

    @PostMapping("/editUser")
    public String editUser(@Validated @ModelAttribute("editableUser")User user, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "admin-commands/edit-user";
        }
        userService.editUser(user);
        model.addAttribute("dataBase", getDataBase());
        return "admin-commands/users";
    }

    @PostMapping("/deleteUser")
    public RedirectView deleteUser(@RequestParam("loginToDelete") String loginToDelete) {
        userService.deleteUserWithLogin(loginToDelete);
        return new RedirectView("/users");
    }

    @ModelAttribute("dataBase")
    public Map<String, User> getDataBase() {
        return userService.getUsers();
    }

    @ModelAttribute("allRoles")
    public List<Role> getAllRoles() {
        return userService.selectRoles();
    }
}
