package by.bar71.web.utils;

import by.bar71.domain.Role;
import by.bar71.domain.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ValidationLogic {

    public static boolean validateUser(User user) {
        return user.getLogin() != null && user.getPassword() != null &&
                user.getEmail() != null && user.getRoles() != null &&
                user.getBirthday() != null && user.getSurname() != null &&
                user.getName() != null && user.getPatronymic() != null;
    }

    public static boolean visitorIsAdmin(HttpServletRequest req) {
        HttpSession session = req.getSession();
        if (session == null) {
            return false;
        }
        User u = (User)session.getAttribute("user");
        if (u == null) {
            return false;
        }
        boolean isAdmin = false;
        for (Role r : u.getRoles()) {
            if (r.getNaming().equals("admin")) {
                isAdmin = true;
                break;
            }
        }
        return isAdmin;
    }

    public static boolean visitorIsLoggedIn(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return session != null && session.getAttribute("user") != null;
    }


}
