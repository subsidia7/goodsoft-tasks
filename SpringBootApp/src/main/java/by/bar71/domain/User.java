package by.bar71.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class User {

    @NotEmpty
    @Size(min = 2, max = 30, message = "{login.size.error}")
    private String login;

    @NotEmpty
    @Size(min = 8, max = 16, message = "{password.size.error}")
    private String password;

    @NotEmpty(message = "{roles.notEmpty.error}")
    private List<Role> roles;

    @Pattern(regexp=".+@.+\\.[a-z]+", message = "{email.pattern.error}")
    private String email;

    @NotNull
    @Size(min = 2, max = 30, message = "{surname.size.error}")
    private String surname;

    @NotNull
    @Size(min = 2, max = 30, message = "{name.size.error}")
    private String name;

    @NotNull
    @Size(min = 2, max = 30, message = "{patronymic.size.error}")
    private String patronymic;

    @Past(message = "{birthday.past.error}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    public User() {

    }

    public User(
                String login,
                String password,
                List<Role> roles,
                String email,
                String surname,
                String name,
                String patronymic,
                Date birthday) {
        this.login = login;
        this.password = password;
        this.roles = roles;
        this.email = email;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setBirthday(String birthday) {
        this.birthday = Date.from(Instant.parse(birthday));
        System.out.println("POST METHOD ADD " + this.birthday + " " + birthday);
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Date getBirthday() {
        return birthday;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (obj instanceof User) {
            return this.login.equals(((User)obj).login);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password);
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(100);
        if (roles != null) {
            for (Role role : roles) {
                res.append(role);
            }
        }

        return login + " " + password + " " +
                res + " " + email + " " +
                surname + " " + name + " " +
                patronymic + " " + birthday;
    }

}
