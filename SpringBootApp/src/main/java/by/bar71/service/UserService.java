package by.bar71.service;

import by.bar71.domain.Role;
import by.bar71.domain.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    User findByLoginAndPassword(String login, String password);

    User findByLogin(String login);

    List<Role> selectRoles();

    List<Role> getUserRoles(String login);

    Map<String, User> getUsers();

    void addUser(User user);

    void editUser(User user);

    void deleteUserWithLogin(String login);

}
