package by.bar71.service;

import by.bar71.data.UserMapper;
import by.bar71.domain.MyUserDetails;
import by.bar71.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userMapper.selectUserByLogin(login);

        if (user == null) {
            throw new UsernameNotFoundException("Not found: " + login);
        }
        return new MyUserDetails(user);
    }
}
