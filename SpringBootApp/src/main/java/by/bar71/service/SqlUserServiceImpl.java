package by.bar71.service;

import by.bar71.data.UserMapper;
import by.bar71.domain.Role;
import by.bar71.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SqlUserServiceImpl implements UserService{

    private final UserMapper userMapper;

    @Autowired
    public SqlUserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public List<Role> selectRoles() {
        return userMapper.selectRoles();
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        return userMapper.selectUserByLoginAndPassword(login, password);
    }

    @Override
    public User findByLogin(String login) {
        return userMapper.selectUserByLogin(login);
    }

    @Override
    public List<Role> getUserRoles(String login) {
        return userMapper.selectUserRoles(login);
    }

    @Override
    public Map<String, User> getUsers() {
        List<User> usersList = userMapper.selectUsers();
        return usersList.stream().collect(Collectors.toMap(User::getLogin, user -> user));
    }

    @Override
    public void addUser(User user) {
        userMapper.insertUser(user);
        for (Role r : user.getRoles()) {
            userMapper.insertUserRoles(user.getLogin(), r.getNaming());
        }
    }

    @Override
    public void editUser(User user) {
        userMapper.updateUser(user);
        userMapper.deleteUserRoles(user.getLogin());
        for (Role r : user.getRoles()) {
            userMapper.insertUserRoles(user.getLogin(), r.getNaming());
        }
    }

    @Override
    public void deleteUserWithLogin(String login) {
        userMapper.deleteUserRoles(login);
        userMapper.deleteUserByLogin(login);
    }

}
