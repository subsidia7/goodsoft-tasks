<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@tag pageEncoding="UTF-8" %>
<div class="footer">
    <p>Copyright <a href="https://vk.com/kheruvimovv" class="logo" target="_blank"><spring:message code="author"/></a> 2023</p>
</div>