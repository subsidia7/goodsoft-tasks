<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@tag pageEncoding="UTF-8" %>

<table>
    <tr>
        <td><strong><spring:message code="label.login"/></strong></td>
        <td><strong><spring:message code="label.password"/></strong></td>
        <td><strong><spring:message code="label.role"/></strong></td>
        <td><strong><spring:message code="label.email"/></strong></td>
        <td><strong><spring:message code="label.surname"/></strong></td>
        <td><strong><spring:message code="label.name"/></strong></td>
        <td><strong><spring:message code="label.patronymic"/></strong></td>
        <td><strong><spring:message code="label.birthday"/></strong></td>
        <td><strong><spring:message code="label.actions"/></strong></td>
    </tr>
    <c:forEach items="${dataBase}" var="entry">
        <tr>
            <td>
                <c:out value="${entry.value.login}"/>
            </td>
            <td>
                <c:out value="${entry.value.password}"/>
            </td>
            <td>
                <c:out value="${entry.value.roles}"/>
            </td>
            <td>
                <c:out value="${entry.value.email}"/>
            </td>
            <td>
                <c:out value="${entry.value.surname}"/>
            </td>
            <td>
                <c:out value="${entry.value.name}"/>
            </td>
            <td>
                <c:out value="${entry.value.patronymic}"/>
            </td>
            <td>
                <fmt:formatDate type="date" value="${entry.value.birthday}" />
            </td>
            <td>
                <form action="deleteUser" method="post">
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                    <input type="hidden" name="loginToDelete" value="${entry.value.login}">
                    <button type="button">
                        <a href="editUser?editableLogin=${entry.value.login}"><spring:message code="label.editUser"/></a>
                    </button>
                    <button type="submit"><spring:message code="label.deleteUser"/></button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>