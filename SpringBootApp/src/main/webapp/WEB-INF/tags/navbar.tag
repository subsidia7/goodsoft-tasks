<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@tag pageEncoding="UTF-8" %>

<div class="topnav">
    <a class="active" href="welcome"><spring:message code="label.welcome"/></a>
    <sec:authorize access="hasRole('admin')">
        <a href="users"><spring:message code="label.users"/></a>
        <a href="addUser"><spring:message code="label.addUser"/></a>
    </sec:authorize>
</div>