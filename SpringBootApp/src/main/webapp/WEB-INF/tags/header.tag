<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@tag pageEncoding="UTF-8" %>
<div class="header">
    <div id="LOGO">BAR71</div>
    <div class="header-right">
        <a href="?lang=en"><img src="images/uk_flag.png"></a>
        <a href="?lang=ru"><img src="images/rus_flag.png"></a>
        <sec:authorize access="isAuthenticated()">
            <form action="login?logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
                <spring:message code="label.hello"/> <sec:authentication property="name"/>.
                <button type="submit" class="btn-link">
                    <spring:message code="label.logout"/>
                </button>
            </form>
        </sec:authorize>
    </div>
</div>