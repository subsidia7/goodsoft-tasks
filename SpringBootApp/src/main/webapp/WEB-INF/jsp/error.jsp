<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title><spring:message code="label.error.errorHeader"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
    <h1><spring:message code="label.error.oops"/> </h1>
    <br/>
    <a href="login"><spring:message code="label.error.login"/></a>
    <br/>
    <a href="welcome"><spring:message code="label.error.welcome"/></a>
<mt:footer/>
</body>
</html>
