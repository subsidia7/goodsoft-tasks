<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title><spring:message code="label.auth"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<mt:header/>

    <form action="login" name="loginForm" method="post">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
        <label>
            <spring:message code="label.login"/>:
            <input type="text" name="username" value="eamatent" required>
        </label>
        <label>
            <spring:message code="label.password"/>:
            <input type="password" name="password" value="aRkMORhV06J65oF" required>
        </label>
        <button type="submit"><spring:message code="label.signin"/></button>
    </form>

    <c:if test="${not empty param.error}">
        <div>
            Invalid username and password.
        </div>
    </c:if>

    <c:if test="${not empty param.logout}">
        <div>
            Your were logout.
        </div>
    </c:if>

<mt:footer/>

</body>
</html>
