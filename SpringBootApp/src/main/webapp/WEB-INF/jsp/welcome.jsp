<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><spring:message code="label.welcome"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <mt:header/>
    <mt:navbar/>
    <p id="greetings">
        <spring:message code="greeting"/> <strong><sec:authentication property="name"/></strong>.
    </p>
    <mt:footer/>
</body>
</html>
