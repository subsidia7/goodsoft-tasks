<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><spring:message code="label.users"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
<mt:navbar/>
<mt:users/>
<mt:footer/>
</body>
</html>
