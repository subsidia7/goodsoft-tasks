<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title><spring:message code="label.addUser"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
<mt:navbar/>
<h2 align="center"><spring:message code="label.enterUser"/></h2>
<form:form action="/addUser" method="post" modelAttribute="user">
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>

    <spring:message code="label.login"/>:
    <form:input path="login" type="text" name="login" value="newUser" required="required"/>
    <form:errors path="login" cssStyle="color: crimson"/>

    <spring:message code="label.password"/>:
    <form:input path="password" type="password" name="password" value="1234" required="required"/>
    <form:errors path="password" cssStyle="color: crimson"/>

    <form:checkboxes path="roles" items="${allRoles}"/>
    <form:errors path="roles" cssStyle="color: crimson"/>

    <spring:message code="label.email"/>:
    <form:input path="email" type="email" name="email" value="newUser@gmail.com" required="required"/>
    <form:errors path="email" cssStyle="color: crimson"/>

    <spring:message code="label.surname"/>:
    <form:input path="surname" type="text" name="surname" value="Userov" required="required"/>
    <form:errors path="surname" cssStyle="color: crimson"/>

    <spring:message code="label.name"/>:
    <form:input path="name" type="text" name="name" value="NewUser" required="required"/>
    <form:errors path="name" cssStyle="color: crimson"/>

    <spring:message code="label.patronymic"/>:
    <form:input path="patronymic" type="text" name="patronymic" value="Userovich" required="required"/>
    <form:errors path="patronymic" cssStyle="color: crimson"/>

    <spring:message code="label.birthday"/>:
    <form:input path="birthday" type="date" name="birthday" value="2001-01-04" required="required"/>
    <form:errors path="birthday" cssStyle="color: crimson"/>

    <button type="submit"><spring:message code="label.addUser"/></button>
</form:form>
<mt:users/>
<mt:footer/>
</body>
</html>
