<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Редактирование данных пользователя</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
<mt:navbar/>
<h2 align="center">Измените нужные данные пользователя <u>${param.visitorLogin}</u></h2>
<form:form action="editUser" method="post" modelAttribute="editableUser">
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>

    <spring:message code="label.login"/>:
    <form:input path="login" type="text" name="login" value="${editableUser.login}" readonly="true" required="true"/>
    <form:errors path="login" cssStyle="color: crimson"/>

    <br/><spring:message code="label.password"/>:
    <form:input path="password" type="password" name="password" value="${editableUser.password}" required="true"/>
    <form:errors path="password" cssStyle="color: crimson"/>

    <form:checkboxes path="roles" items="${allRoles}"/>
    <form:errors path="roles" cssStyle="color: crimson"/>

    <br/><spring:message code="label.email"/>::
    <form:input path="email" type="email" name="email" value="${editableUser.email}" required="true"/>
    <form:errors path="email" cssStyle="color: crimson"/>

    <br/><spring:message code="label.surname"/>::
    <form:input path="surname" type="text" name="surname" value="${editableUser.surname}" required="true"/>
    <form:errors path="surname" cssStyle="color: crimson"/>

    <br/><spring:message code="label.name"/>::
    <form:input path="name" type="text" name="name" value="${editableUser.name}" required="true"/>
    <form:errors path="name" cssStyle="color: crimson"/>

    <br/><spring:message code="label.patronymic"/>::
    <form:input path="patronymic" type="text" name="patronymic" value="${editableUser.patronymic}" required="true"/>
    <form:errors path="patronymic" cssStyle="color: crimson"/>

    <br/><spring:message code="label.birthday"/>::
    <form:input path="birthday" type="date" name="birthday" value="${editableUser.birthday}" required="true"/>
    <form:errors path="birthday" cssStyle="color: crimson"/>

    <button type="submit">Изменить</button>
</form:form>
<h2 align="center">Текущий список пользователей</h2>
<mt:users/>
<mt:footer/>
</body>
</html>
