package by.dima;

import by.dima.types.MyType;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        // 1
        System.out.println("Множество из объектов String");
        Set<String> setOfS = new HashSet<String>();
        setOfS.add("One");
        setOfS.add("Two");
        setOfS.add("Free");
        setOfS.add("Four");
        setOfS.add("Five");
        for (String s : setOfS) {
            System.out.println(s);
        }
        System.out.println("Добавим существующий элемент Five");
        setOfS.add("Five");
        for (Iterator<String> i = setOfS.iterator(); i.hasNext(); ) {
            System.out.println(i.next());
        }

        System.out.println("Множество из объектов пользовательского класса");
        Set<MyType> setOfMyType = new HashSet<MyType>();
        setOfMyType.add(new MyType("Q", 1));
        setOfMyType.add(new MyType("W", 2));
        setOfMyType.add(new MyType("E", 3));
        setOfMyType.add(new MyType("R", 4));
        setOfMyType.add(new MyType("T", 5));
        for (MyType mt : setOfMyType) {
            System.out.println(mt);
        }
        System.out.println("Добавим существующий элемент {W, 2}");
        setOfMyType.add(new MyType("W", 2));
        // Если не перегрузить методы equals() и hashCode(),
        // равные элементы все равно будут добавлены в множество
        for (Iterator<MyType> i = setOfMyType.iterator(); i.hasNext();) {
            System.out.println(i.next());
        }
        //2
        System.out.println("Односвязный список объектов String");
        List<String> lS = new LinkedList<String>();
        lS.add("One");
        lS.add("Two");
        lS.add("Three");
        for (Iterator<String> i = lS.iterator(); i.hasNext(); ) {
            System.out.println(i.next());
        }
        for (ListIterator<String> i = lS.listIterator(); i.hasNext(); ) {
            System.out.println(i.next());
            if (i.nextIndex() == 2) {
                i.add("NEW ELEM");
            }
        }
        for (ListIterator<String> i = lS.listIterator(); i.hasNext(); ) {
            System.out.println(i.next());
            if (i.nextIndex() == 3) {
                i.remove();
            }
        }
        System.out.println("После удаления");
        for (String s : lS)
        {
            System.out.println(s);
        }
        /*
        Вывод по первым двум пунктам:
        1. Использована структура данных множество, а именно HashSet. При добавлении элемента
        сравниваются хеш-коды, если есть одинаковые коды, то элемент добавлен не будет, потому
        что уже существует. Но даже если хеш код одинаков, все равно вызовется метод equals(),.
        поэтому его тоже надо перегрузить.
        Для вывода элементов был использован for с итераторам типа Iterator.
        Также была использована удобная запись цикла for - foreach.
        2. Использована структура данных Linked List.
        Нужно было выводить данные и при этом вставлять либо удалять элемент, т.е. изменять данные.
        Поэтому был использован итератор ListIterator.
        add добавляет перед элементом полученным последним next или после последнего prev
        remove удаляет элемент последний раз возвращенный next или prev.
         */

        //3
        // Мапа ключ - номер телефона, значение - Имя абонента
        HashMap<String, String> map = new HashMap<>();
        map.put("+37529777777", "Alexandr");
        map.put("+37533123123", "Elena");
        map.put("+79061623121", "Stepan");
        map.put("+47533123123", "Elena");
        // Вывод с помощью map.toString();
        System.out.println(map);
        // Добавили пару с уже имеющимся ключом
        map.put("+79061623121", "Igor");
        // Вывод через map.entrySet(), значение уже по имеющемуся ключу было заменено
        for (Map.Entry<String, String> e : map.entrySet()) {
            System.out.println(e);
        }

        // Мапа ключ - пользовательский тип данных, значение - строка
        HashMap<MyType, String> map2 = new HashMap<>();
        map2.put(new MyType("a", 1), "xyz1");
        map2.put(new MyType("b", 2), "xyz2");
        map2.put(new MyType("c", 3), "xyz3");
        System.out.println(map2);
        map2.put(new MyType("b", 2), "ZAMENA");
        for (Map.Entry<MyType, String> e : map2.entrySet()) {
            System.out.println(e);
        }
        // 4
        // Отсортированное множество
        SortedSet<String> s1 = new TreeSet<>();
        s1.add("c");
        s1.add("a");
        s1.add("q");
        s1.add("b");
        System.out.println(s1);
        Set<MyType> s2 = new TreeSet<>();
        s2.add(new MyType("q", 1));
        s2.add(new MyType("h", 3));
        s2.add(new MyType("a", 1));
        s2.add(new MyType("h", 1));
        System.out.println(s2);

        // 5
        // Отсортированная мапа
        SortedMap<String, String> map1 = new TreeMap<>();
        map1.put("+37529777777", "Alexandr");
        map1.put("+47533123123", "Elena");
        map1.put("+37533123123", "Elena");
        map1.put("+79061623121", "Stepan");
        System.out.println(map1);

        SortedMap<MyType, String> mapp = new TreeMap<>();
        mapp.put(new MyType("a", 1), "xyz1");
        mapp.put(new MyType("c", 2), "xyz3");
        mapp.put(new MyType("b", 3), "xyz2");
        System.out.println(map2);

    }
}