package by.dima.types;

import java.util.Objects;

public class MyType implements Comparable<MyType> {
    private Integer x;
    private String s;
    public MyType(String s, int x) {
        this.s = s;
        this.x = x;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MyType mta)) {
            return false;
        }
        return mta.x == this.x && mta.s.equals(this.s);
    }
    @Override
    public int hashCode() {
       return Objects.hash(x, s);
    }
    @Override
    public String toString() {
        return "MyType object: " + s + " " + x;
    }
    @Override
    public int compareTo(MyType other){
        int last = this.s.compareTo(other.s);
        return last == 0 ? this.x.compareTo(other.x) : last;
    }
}
