import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree
} from "@angular/router";
import {inject} from "@angular/core";

export const authentificationGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): boolean | UrlTree => {
  if(sessionStorage.getItem("user")) {
    return true;
  }
  return inject(Router).createUrlTree(['/login']);
}

