import {Role} from "./role";

export interface User {
  id: number;
  username: string;
  password: string;
  email: string;
  name: string;
  surname: string;
  patronymic: string;
  roles: Role[];
  birthday: Date;
}
