import {Component, inject, OnInit} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {ReactiveFormsModule, Validators, FormBuilder, FormArray, FormGroup} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {NgFor, NgIf} from "@angular/common";
import {ActivatedRoute} from "@angular/router";
import {User} from "../../domain/user";
import {validateOneRoleRequired} from "../../validators/roles.validator";
import {validatePast} from "../../validators/birthday.validator";
import {CardModule} from "primeng/card";
import {CheckboxModule} from "primeng/checkbox";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {CalendarModule} from "primeng/calendar";
import {Role} from "../../domain/role";
import {UserFormComponent} from "../user-form/user-form.component";

@Component({
  selector: 'app-add-user',
  standalone: true,
  imports: [
    NavbarComponent,
    ReactiveFormsModule,
    NgIf,
    NgFor,
    CardModule,
    CheckboxModule,
    ButtonModule,
    InputTextModule,
    CalendarModule,
    UserFormComponent
  ],
  templateUrl: './edit-user.component.html',
  styleUrl: './edit-user.component.css'
})
export class EditUserComponent{

  route: ActivatedRoute = inject(ActivatedRoute);
  userService = inject(UserService);
  userId!: number;

  constructor() {
    this.userId = parseInt(this.route.snapshot.params['id'], 10);
  }

}
