import {Component, inject, Input, OnInit} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {ReactiveFormsModule, Validators, FormBuilder, FormControl, FormArray, FormGroup} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {NgFor, NgIf} from "@angular/common";
import {validateOneRoleRequired} from "../../validators/roles.validator";
import {validatePast} from "../../validators/birthday.validator";
import { CheckboxModule } from 'primeng/checkbox';
import {InputTextModule} from "primeng/inputtext";
import { ButtonModule } from 'primeng/button';
import {CardModule} from "primeng/card";
import {CalendarModule} from "primeng/calendar";
import {Role} from "../../domain/role";
import {User} from "../../domain/user";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-form',
  standalone: true,
  imports: [
    NavbarComponent,
    ReactiveFormsModule,
    NgIf,
    NgFor,
    CheckboxModule,
    InputTextModule,
    ButtonModule,
    CardModule,
    CalendarModule
  ],
  templateUrl: './user-form.component.html',
  styleUrl: './user-form.component.css'
})
export class UserFormComponent{

  formBuilder = inject(FormBuilder);
  userService = inject(UserService);
  router = inject(Router);
  allRoles: Role[] = []

  @Input({required: true}) submitFn!: (user: User) => Observable<User>;

  @Input() userId: number | null = null;

  defaultUser: User | null = null;

  @Input({required: true}) submitLabel!: string;

  dataLoaded = false;

  userForm = this.formBuilder.group({
      username: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(14)]
      ],
      email: ['', [
        Validators.required,
        Validators.email],
      ],
      name: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)]
      ],
      surname: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)]
      ],
      patronymic: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)]
      ],
      birthday: [new Date(Date.now()), [
        Validators.required,
        validatePast()]
      ],
      roles: this.formBuilder.array([], [validateOneRoleRequired()])
    }
  )



  constructor() {
    this.userService.getAllRoles().subscribe({
      next: next => {
        this.allRoles = next;
        if (this.userId) {
          this.userService.getUserById(this.userId).subscribe({
            next: value => {
              this.defaultUser = value;
              this.setupForm();
            },
            error: err => console.log(err["message"])
          })
        } else {
          this.setupForm();
        }
      },
      error: err => console.log(err["message"])
    });
  }

  setupForm() {
    this.username?.setValue(this.defaultUser?.username ?? this.defaultUser?.username ?? 'example_username');
    this.password?.setValue(this.defaultUser?.password ?? 'example_pass');
    this.email?.setValue(this.defaultUser?.email ?? 'example@gmail.com');
    this.name?.setValue(this.defaultUser?.name ?? 'example_name');
    this.surname?.setValue(this.defaultUser?.surname ?? 'example_surname');
    this.patronymic?.setValue(this.defaultUser?.patronymic ?? 'example_patronymic');
    if (this.defaultUser?.birthday) {
      this.birthday?.setValue(new Date(this.defaultUser.birthday));
    } else {
      this.birthday?.setValue(new Date('2003-03-15'));
    }


    const fa: FormArray = this.userForm.get('roles') as FormArray;
    for (const role of this.allRoles) {
      fa.push(this.formBuilder.control(
        this.defaultUser?.roles.some(
          r => r.naming === role.naming) ?? false
      ));
    }
    this.dataLoaded = true;
  }

  get username() {
    return this.userForm.get('username');
  }

  get password() {
    return this.userForm.get('password');
  }

  get email() {
    return this.userForm.get('email');
  }

  get surname() {
    return this.userForm.get('surname');
  }

  get name() {
    return this.userForm.get('name');
  }

  get patronymic() {
    return this.userForm.get('patronymic');
  }

  get birthday() {
    return this.userForm.get('birthday');
  }

  get roles() {
    return this.userForm.get('roles') as FormArray;
  }

  submitUser(): void {
    let userRoles = [];
    for (let i = 0; i < this.allRoles.length; i++) {
      if(this.roles.at(i).value) {
        userRoles.push(this.allRoles[i]);
      }
    }
    const user = {... this.userForm.value};
    user.roles = userRoles;
    const u: User = user as unknown as User;
    u.id = this.defaultUser?.id ?? -1;
    this.submitFn(u).subscribe({
      next: value => this.router.navigate(["/users"]),
      error: err => console.log(err)
    });
  }

}
