import {Component, inject, OnInit} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {ReactiveFormsModule, Validators, FormBuilder, FormControl, FormArray} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {NgFor, NgIf} from "@angular/common";
import {validateOneRoleRequired} from "../../validators/roles.validator";
import {validatePast} from "../../validators/birthday.validator";
import { CheckboxModule } from 'primeng/checkbox';
import {InputTextModule} from "primeng/inputtext";
import { ButtonModule } from 'primeng/button';
import {CardModule} from "primeng/card";
import {CalendarModule} from "primeng/calendar";
import {Role} from "../../domain/role";
import {UserFormComponent} from "../user-form/user-form.component";

@Component({
  selector: 'app-add-user',
  standalone: true,
  imports: [
    NavbarComponent,
    ReactiveFormsModule,
    NgIf,
    NgFor,
    CheckboxModule,
    InputTextModule,
    ButtonModule,
    CardModule,
    CalendarModule,
    UserFormComponent
  ],
  templateUrl: './add-user.component.html',
  styleUrl: './add-user.component.css'
})
export class AddUserComponent{

  userService = inject(UserService);

}
