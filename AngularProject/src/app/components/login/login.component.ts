import {Component, inject} from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {NgIf} from "@angular/common";
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import {AuthRequest} from "../../domain/auth-request";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule, NgIf, CardModule, InputTextModule, FormsModule, ButtonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  userService: UserService = inject(UserService);
  router = inject(Router);

  loginForm = new FormGroup({
      username: new FormControl('eamatent', Validators.required),
      password: new FormControl('aRkMORhV06J65oF', Validators.required)
    }
  )

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  submitAuth(): void {
    const authRequest = this.loginForm.value as AuthRequest;
    this.userService.auth(authRequest)
      .subscribe({
        next: value => {
          console.log(value);
          sessionStorage.setItem("jwt", value.jwt);
          sessionStorage.setItem("user", JSON.stringify(value.user));
          this.userService.updateHttpOptions();
          this.router.navigate(['/welcome']);
        },
        error: err => console.log(err)
      })
  }
}
