import {inject, Injectable, OnInit} from '@angular/core';
import {User} from "../domain/user";
import {Role} from "../domain/role";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthRequest} from "../domain/auth-request";
import {AuthResponse} from "../domain/auth-response";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  http = inject(HttpClient);

  baseUrl = "http://localhost:8080"

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
      'Authorization': `Bearer ${sessionStorage.getItem('jwt')}`}
    )
  };

  updateHttpOptions() {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'Authorization': `Bearer ${sessionStorage.getItem('jwt')}`}
      )
    };
  }

  auth(authRequest: AuthRequest) {
    return this.http.post<AuthResponse>(`${this.baseUrl}/auth`, authRequest);
  }

  getUserByUsername(username: string): Observable<User> {
    return this.http.get<User>(
      `${this.baseUrl}/users/username/${username}`,
      this.httpOptions);
  }

  getSessionUsername(): string {
    const user = JSON.parse(sessionStorage.getItem("user")!) as User;
    return user.username;
  }

  getAllUsers(): Observable<User[]> {
    console.log("GET ALL USERS" + this.httpOptions);
    console.log(sessionStorage.getItem("jwt"));
    return this.http.get<User[]>(`${this.baseUrl}/users`, this.httpOptions);
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}/users`, user, this.httpOptions);
  }

  editUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.baseUrl}/users`, user, this.httpOptions);
  }

  deleteUser(id: number) {
    return this.http.delete(`${this.baseUrl}/users/${id}`, this.httpOptions);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/users/${id}`, this.httpOptions);
  }

  getAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(`${this.baseUrl}/roles`, this.httpOptions);
  }

  isAdmin(user: User): boolean {
    if (user.roles.some(r => r.naming === "admin")) {
      return true;
    }
    return false;
  }

}
