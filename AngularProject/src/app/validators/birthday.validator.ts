import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function validatePast(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if(Date.now() - new Date(Date.parse(control.value)).getTime() < 0) {
      return {past: {value: "Birthday can't be in future"}}
    }
    return null;
  }
}
