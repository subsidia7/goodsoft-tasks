package by.bar71.data;

import by.bar71.domain.Role;
import by.bar71.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    User selectUserByUsernameAndPassword(String username, String password);

    User selectUserByUsername(String username);

    User selectUserById(Long id);

    List<Role> selectUserRoles(String username);

    List<User> selectAllUsers();

    List<Role> selectAllRoles();

    void insertUser(User user);

    void insertUserRole(Long user_id, Long role_id);

    void updateUser(User user);

    void deleteUserById(Long id);

    void deleteUserRoles(Long id);

    Role selectRoleById(Long id);

}
