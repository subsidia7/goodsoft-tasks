package by.bar71.web.controllers;

import by.bar71.web.auth.AuthRequest;
import by.bar71.web.auth.AuthResponse;
import by.bar71.service.UserService;
import by.bar71.web.auth.jwt.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final JwtUtil jwtUtil;
    private final UserService userService;

    @PostMapping
    public AuthResponse authenticate(
            @RequestBody AuthRequest request
    ) {
        final UserDetails user = userDetailsService.loadUserByUsername(request.getUsername());
        System.out.println(user);
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
        );
        return new AuthResponse(jwtUtil.generateToken(user), userService.selectUserByUsername(request.getUsername()));
    }
}
