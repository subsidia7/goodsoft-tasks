package by.bar71.web.controllers;

import by.bar71.domain.Role;
import by.bar71.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "roles")
public class RolesController {

    UserService userService;

    @Autowired
    public RolesController(UserService userService) {this.userService = userService;}

    /***
     * @return List of all Roles from Database
     */
    @GetMapping
    public List<Role> getRoles() {
        return userService.selectAllRoles();
    }

    /***
     * @return Role with specified id
     */
    @GetMapping("{id}")
    public Role getRole(@PathVariable Long id) {
        return userService.selectRoleById(id);
    }
}
