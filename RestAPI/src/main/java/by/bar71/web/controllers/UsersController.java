package by.bar71.web.controllers;

import by.bar71.domain.User;
import by.bar71.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "users")
public class UsersController {

    private final UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    /***
     * @return List of users from Database
     */
    @GetMapping
    public List<User> selectAllUsers() {
        return userService.selectAllUsers();
    }

    /***
     * @param id User id
     * @return User with passed id
     */
    @GetMapping("{id}")
    public User selectUserById(@PathVariable Long id) {
        return userService.selectUserById(id);
    }

    /***
     * @param username
     * @return User with passed login
     */
    @GetMapping("/username/{username}")
    public User selectUserByUsername(@PathVariable String username) {
        return userService.selectUserByUsername(username);
    }

    /***
     * @param user The user to be inserted
     */
    @PostMapping
    public void insertUser(@RequestBody User user) {
        userService.insertUser(user);
    }

    /***
     * @param user The user to be updated
     */
    @PutMapping
    public void updateUser(@RequestBody User user) {
        userService.updateUser(user);
    }

    /***
     * @param id The id of User to delete
     */
    @DeleteMapping("{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUserWithId(id);
    }

}