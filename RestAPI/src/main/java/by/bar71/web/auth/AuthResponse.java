package by.bar71.web.auth;

import by.bar71.domain.User;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class AuthResponse {

    String jwt;

    User user;

}
