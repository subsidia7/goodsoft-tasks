package by.bar71.service;

import by.bar71.data.UserMapper;
import by.bar71.domain.Role;
import by.bar71.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SqlUserServiceImpl implements UserService{

    private final UserMapper userMapper;

    private final ApplicationContext context;

    @Autowired
    public SqlUserServiceImpl(
            UserMapper userMapper,
            ApplicationContext context
    ) {
        this.userMapper = userMapper;
        this.context = context;
    }

    @Override
    public List<Role> selectAllRoles() {
        return userMapper.selectAllRoles();
    }

    @Override
    public User selectUserByUsernameAndPassword(String username, String password) {
        User user = userMapper.selectUserByUsernameAndPassword(username, password);
        if (user == null) {
            throw new IllegalStateException(String.format("User with username = %s and password = %s not found", username, password));
        }
        return user;
    }

    @Override
    public User selectUserByUsername(String username) {
        User user = userMapper.selectUserByUsername(username);
        if (user == null) {
            throw new IllegalStateException(String.format("User with username = %s not found", username));
        }
        return user;
    }

    @Override
    public User selectUserById(Long id) {
        User user = userMapper.selectUserById(id);
        if (user == null) {
            throw new IllegalStateException(String.format("User with id = %d not found", id));
        }
        return user;
    }

    @Override
    public List<Role> selectUserRoles(String username) {
        return userMapper.selectUserRoles(username);
    }

    @Override
    public List<User> selectAllUsers() {
        return userMapper.selectAllUsers();
    }

    @Override
    public void insertUser(User user) {
        try {
            user.setPassword(context.getBean(PasswordEncoder.class).encode(user.getPassword()));
            userMapper.insertUser(user);
            for (Role r : user.getRoles()) {
                userMapper.insertUserRole(
                        userMapper.selectUserByUsername(user.getUsername()).getId(),
                        r.getId()
                );
            }
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override
    public void updateUser(User user) {
        try {
            userMapper.updateUser(user);
            userMapper.deleteUserRoles(user.getId());
            for (Role r : user.getRoles()) {
                userMapper.insertUserRole(user.getId(), r.getId());
            }
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override
    public void deleteUserWithId(Long id) {
        userMapper.deleteUserRoles(id);
        userMapper.deleteUserById(id);
    }

    @Override
    public Role selectRoleById(Long id) {
        return userMapper.selectRoleById(id);
    }

}
