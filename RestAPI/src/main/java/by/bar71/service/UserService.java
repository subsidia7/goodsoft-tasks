package by.bar71.service;

import by.bar71.domain.Role;
import by.bar71.domain.User;

import java.util.List;

public interface UserService {

    User selectUserByUsernameAndPassword(String username, String password);

    User selectUserByUsername(String username);

    List<Role> selectAllRoles();

    User selectUserById(Long id);

    List<Role> selectUserRoles(String username);

    List<User> selectAllUsers();

    void insertUser(User user);

    void updateUser(User user);

    void deleteUserWithId(Long id);

    Role selectRoleById(Long id);

}
