package by.bar71.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {

    private Long id;

    @NotEmpty(message = "{naming.notEmpty.error}")
    private String naming;

    public Role() {

    }

    public Role(Long id, String naming) {
        this.id = id;
        this.naming = naming;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNaming(String naming) {
        this.naming = naming;
    }

    public Long getId() {
        return id;
    }

    public String getNaming() {
        return naming;
    }

    @Override
    public String toString() {
        return naming;
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + naming;
    }
}
