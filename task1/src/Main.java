import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        File fin, fout;
        String filepath;
        while (true) {
            System.out.println("Enter path to file:");
            filepath = s.nextLine();
            fin = new File(filepath);
            if (fin.exists()) {
                break;
            }
            System.out.println("File does not exist");
        }

        System.out.println("Enter destination path:");
        String destinationPath = s.nextLine();
        fout = new File(destinationPath);
        if (fout.exists()) {
            System.out.println("File already exists. Would you want to rewrite it? (Y/N)");
            String tmp = s.nextLine();
            if (tmp.equals("N")) {
                return;
            }
        }
        System.out.println("Choose copy method(1-IO, 2-NIO, 3-NIO2):");
        String tmp = s.nextLine();
        if (tmp.equals("1")) {
            ICopier c = new CopierIO();
            c.copy(fin, fout);
        }
        else if (tmp.equals("2")) {
            ICopier c = new CopierNIO();
            c.copy(fin, fout);
        }
        else if (tmp.equals("3")) {
            ICopier c = new CopierNIO2();
            c.copy(fin, fout);
        }
    }
}
