import java.io.*;
import java.nio.channels.FileChannel;

public class CopierIO implements ICopier {
    public void copy(File from, File to) {
        byte[] buffer = new byte[1024];
        int bytesRead = 0;
        for (int i = 0; i < 10000; ++i)
        {
            try (FileInputStream reader  = new FileInputStream(from);
                 FileOutputStream writer = new FileOutputStream(to)) {
                while(true) {
                    bytesRead = reader.read(buffer, 0, buffer.length);
                    if (bytesRead == -1) {
                        break;
                    }
                    writer.write(buffer, 0, bytesRead);
                }
            }
            catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
