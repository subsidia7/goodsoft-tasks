import java.io.*;
import java.nio.channels.FileChannel;

public class CopierNIO implements ICopier {
    public void copy(File from, File to) {
        try(FileChannel srcFileChannel  = new FileInputStream(from).getChannel();
            FileChannel dstFileChannel = new FileOutputStream(to).getChannel()){
            // 2. Размер текущего FileChannel
            long count = srcFileChannel.size();
            while(count > 0){
                /**=============================================================
                 * 3. Записать байты из FileChannel исходного файла в целевой FileChannel
                 * 1. srcFileChannel.position (): начальная позиция в исходном файле не может быть отрицательной
                 * 2. count: максимальное количество переданных байтов, не может быть отрицательным
                 * 3. dstFileChannel: целевой файл
                 *==============================================================*/
                long transferred = srcFileChannel.transferTo(srcFileChannel.position(),
                        count, dstFileChannel);
                // 4. После завершения переноса измените положение исходного файла на новое место
                srcFileChannel.position(srcFileChannel.position() + transferred);
                // 5. Рассчитаем, сколько байтов осталось перенести
                count -= transferred;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
