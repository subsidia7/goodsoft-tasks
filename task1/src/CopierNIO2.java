import java.io.File;
import java.io.IOException;
import java.nio.file.*;
public class CopierNIO2 implements ICopier {
    public void copy(File from, File to) {
        try {
            Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
