import java.io.File;

public interface ICopier {
    void copy(File from, File to);
}
