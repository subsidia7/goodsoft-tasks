<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<html>
<head>
    <title>Добавить пользователя</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
<mt:navbar/>
<h2 align="center">Введите данные пользователя</h2>
<form action="controller?command=addUser" method="post">
    Логин:
    <input type="text" name="login" value="newUser" required>
    Пароль:
    <input type="password" name="password" value="1234" required>

    Роль:
    <input type="checkbox" id="admin" name="admin">
    <label for="admin"> Admin</label>
    <input type="checkbox" id="tester" name="tester">
    <label for="tester"> Tester</label>
    <input type="checkbox" id="director" name="director">
    <label for="director"> Director</label>
    <input type="checkbox" id="security" name="security">
    <label for="security"> Security</label>
    <input type="checkbox" id="manager" name="manager">
    <label for="manager"> Manager</label><br>

    Email:
    <input type="email" name="email" value="newUser@gmail.com" required>
    Фамилия:
    <input type="text" name="surname" value="Userov" required>
    Имя:
    <input type="text" name="name" value="NewUser" required>
    Отчество:
    <input type="text" name="patronymic" value="Userovich" required>
    Дата рождения:
    <input type="text" name="birthday" value="2001-01-04" required>
    <button type="submit">Добавить</button>
</form>
<mt:users/>
<mt:footer/>
</body>
</html>
