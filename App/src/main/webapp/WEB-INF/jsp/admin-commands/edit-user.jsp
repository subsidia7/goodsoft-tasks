<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<html>
<head>
    <title>Редактирование данных пользователя</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
<mt:navbar/>
<h2 align="center">Измените нужные данные пользователя <u>${param.visitorLogin}</u></h2>
<form action="controller?command=editUser" method="post">
    <c:set scope="page" var="editableUser" value="${applicationScope.dataBase[param.visitorLogin]}"></c:set>
    Логин:
    <input type="text" name="login" value="${editableUser.login}" required readonly>
    <br/>Пароль:
    <input type="password" name="password" value="${editableUser.password}" required>

    <br/>Email:
    <input type="email" name="email" value="${editableUser.email}" required>
    <br/>Фамилия:
    <input type="text" name="surname" value="${editableUser.surname}" required>
    <br/>Имя:
    <input type="text" name="name" value="${editableUser.name}" required>
    <br/>Отчество:
    <input type="text" name="patronymic" value="${editableUser.patronymic}" required>
    <br/>Дата рождения:
    <input type="text" name="birthday" value="${editableUser.birthday}" required>
    <button type="submit">Изменить</button>
</form>
<h2 align="center">Текущий список пользователей</h2>
<mt:users/>
<mt:footer/>
</body>
</html>
