<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Список пользователей</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<mt:header/>
<mt:navbar/>
<mt:users/>
<mt:footer/>
</body>
</html>
