<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Главная страница</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <mt:header/>
    <mt:navbar/>
    <p id="greetings">
        Спасибо за посещение нашего ресурса дорогой, <strong>${sessionScope.user.login}</strong>.
    </p>
    <mt:footer/>
</body>
</html>
