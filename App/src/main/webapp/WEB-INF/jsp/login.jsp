<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt" %>
<html>
<head>
    <title>Авторизация</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="header">
    <div id="LOGO">BAR71</div>
</div>

<form action="controller" name="loginForm" method="post">
    <input type="hidden" name="command" value="login">
    <label>
        Логин:
        <input type="text" name="login" value="eamatent" required>
    </label>
    <label>
        Пароль:
        <input type="password" name="password" value="aRkMORhV06J65oF" required>
    </label>
    <button type="submit">Войти</button>
</form>

<mt:footer/>

</body>
</html>
