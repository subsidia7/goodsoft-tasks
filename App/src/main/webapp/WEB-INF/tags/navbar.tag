<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag pageEncoding="UTF-8" %>

<div class="topnav">
    <a class="active" href="welcome">Главная</a>
    <c:forEach items="${sessionScope.user.roles}" var="role">
        <c:if test="${role == 'admin'}">
            <a href="users">Пользователи</a>
            <a href="add-user">Добавить пользователя</a>
        </c:if>
    </c:forEach>

</div>