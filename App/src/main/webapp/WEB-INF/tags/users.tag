<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag pageEncoding="UTF-8" %>

<table>
    <tr>
        <td><strong>Логин</strong></td>
        <td><strong>Пароль</strong></td>
        <td><strong>Роль</strong></td>
        <td><strong>Email</strong></td>
        <td><strong>Фамилия</strong></td>
        <td><strong>Имя</strong></td>
        <td><strong>Отчество</strong></td>
        <td><strong>День рождения</strong></td>
        <td><strong>Действия</strong></td>
    </tr>
    <c:forEach items="${applicationScope.dataBase}" var="entry">
        <tr>
            <td>
                <c:out value="${entry.value.login}"/>
            </td>
            <td>
                <c:out value="${entry.value.password}"/>
            </td>
            <td>
                <c:out value="${entry.value.roles}"/>
            </td>
            <td>
                <c:out value="${entry.value.email}"/>
            </td>
            <td>
                <c:out value="${entry.value.surname}"/>
            </td>
            <td>
                <c:out value="${entry.value.name}"/>
            </td>
            <td>
                <c:out value="${entry.value.patronymic}"/>
            </td>
            <td>
                <c:out value="${entry.value.birthday}"/>
            </td>
            <td>
                <form action="controller?command=deleteUser" method="post">
                    <input type="hidden" name="visitorLogin" value="${entry.value.login}">
                    <button type="submit" formaction="edit-user?visitorLogin=${entry.value.login}">Редактировать</button>
                    <button type="submit">Удалить</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>