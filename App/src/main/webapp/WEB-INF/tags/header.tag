<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag pageEncoding="UTF-8" %>
<div class="header">
    <div id="LOGO">BAR71</div>
    <div class="header-right">
        Привет, ${sessionScope.user.login}. <a href="controller?command=logout">Выйти.</a>
    </div>
</div>