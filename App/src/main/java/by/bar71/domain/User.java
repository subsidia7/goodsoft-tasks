package by.bar71.domain;

import java.util.ArrayList;
import java.util.Objects;

public class User {
    private String login;
    private String password;
    private ArrayList<String> roles;
    private String email;
    private String surname;
    private String name;
    private String patronymic;
    private String birthday;
    public User() {

    }
    public User(String login,
                String password,
                ArrayList<String> roles,
                String email,
                String surname,
                String name,
                String patronymic,
                String birthday) {
        this.login = login;
        this.password = password;
        this.roles = roles;
        this.email = email;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
    }
    public ArrayList<String> getRoles() {
        return roles;
    }
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public String getEmail() {
        return email;
    }
    public String getSurname() {
        return surname;
    }
    public String getName() {
        return name;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public String getBirthday() {
        return birthday;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (obj instanceof User u) {
            return this.login.equals(u.login);
        }
        return false;
    }
    @Override
    public int hashCode() {
        return Objects.hash(login, password);
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(100);
        for (String role : roles) {
            res.append(role);
        }
        return login + " " + password + " " +
                res + " " + email + " " +
                surname + " " + name + " " +
                patronymic + " " + birthday;
    }
}
