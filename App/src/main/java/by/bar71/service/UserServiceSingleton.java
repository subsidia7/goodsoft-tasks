package by.bar71.service;

public class UserServiceSingleton {
    private static UserServiceSingleton instance = null;
    private UserService userService = null;
    private UserServiceSingleton() {
        userService = new SqlUserServiceImpl();
    }
    public static UserService getValue() {
        if (instance == null) {
            instance = new UserServiceSingleton();
        }
        return instance.userService;
    }
}
