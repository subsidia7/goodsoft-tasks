package by.bar71.service;

import by.bar71.dao.UserDaoSingleton;
import by.bar71.domain.User;

import java.util.ArrayList;
import java.util.HashMap;

public class SqlUserServiceImpl implements UserService{
    @Override
    public User findByLoginAndPassword(String login, String password) {
        return UserDaoSingleton.getValue().findByLoginAndPassword(login, password);
    }

    @Override
    public User findByLogin(String login) {
        return UserDaoSingleton.getValue().findByLogin(login);
    }

    @Override
    public ArrayList<String> getUserRoles(String login) {
        return UserDaoSingleton.getValue().getUserRoles(login);
    }

    @Override
    public HashMap<String, User> getUsers() {
        return UserDaoSingleton.getValue().getUsers();
    }

    @Override
    public boolean addUser(User user) {
        return UserDaoSingleton.getValue().addUser(user);
    }

    @Override
    public boolean editUser(User user) {
        return UserDaoSingleton.getValue().editUser(user);
    }

    @Override
    public boolean deleteUserWithLogin(String login) {
        return UserDaoSingleton.getValue().deleteUserWithLogin(login);
    }
}
