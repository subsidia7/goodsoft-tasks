package by.bar71.dao;

import by.bar71.data.DbConnector;
import by.bar71.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class SqlUserDao implements UserDao{
    public User findByLoginAndPassword(String login, String password) {
        String query = "SELECT * FROM users WHERE login = ? AND password = ?";
        User user = null;
        try (PreparedStatement find = DbConnector.get().prepareStatement(query)) {
            find.setString(1, login);
            find.setString(2, password);
            ResultSet rs = find.executeQuery();
            while (rs.next()) {
                user = new User(
                        rs.getString("login"),
                        rs.getString("password"),
                        getUserRoles(login),
                        rs.getString("email"),
                        rs.getString("surname"),
                        rs.getString("name"),
                        rs.getString("patronymic"),
                        rs.getString("birthday")
                );
            }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return user;
    }
    public User findByLogin(String login) {
        String query = "SELECT * FROM users WHERE login = ?";
        User user = null;
        try (PreparedStatement find = DbConnector.get().prepareStatement(query)) {
            find.setString(1, login);
            ResultSet rs = find.executeQuery();
            while (rs.next()) {
                user = new User(
                        rs.getString(1),
                        rs.getString(2),
                        getUserRoles(login),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                );
            }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return user;
    }

    @Override
    public ArrayList<String> getUserRoles(String login) {
        String query = "select naming from users " +
                "join users_roles ON users.id = users_roles.user_id " +
                "join roles ON roles.id = users_roles.role_id " +
                "WHERE login = ?";
        ArrayList<String> roles = new ArrayList<>();
        try (PreparedStatement find = DbConnector.get().prepareStatement(query)) {
            find.setString(1, login);
            ResultSet rs = find.executeQuery();
            while (rs.next()) {
                roles.add(rs.getString(1));
            }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return roles;
    }

    @Override
    public HashMap<String, User> getUsers() {
        String query = "SELECT * FROM users";
        User user = null;
        HashMap<String, User> map = new HashMap<>();
        try (PreparedStatement find = DbConnector.get().prepareStatement(query)) {
            ResultSet rs = find.executeQuery();
            while (rs.next()) {
                user = new User(
                        rs.getString("login"),
                        rs.getString("password"),
                        getUserRoles(rs.getString("login")),
                        rs.getString("email"),
                        rs.getString("surname"),
                        rs.getString("name"),
                        rs.getString("patronymic"),
                        rs.getString("birthday")
                );
                map.put(user.getLogin(), user);
            }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return map;
    }
    public boolean addUser(User user) {
        String query = "INSERT INTO users " +
                "(id, login, password, email, surname, name, patronymic, birthday)" +
                "VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
        String query2 = "INSERT INTO users_roles " +
                "(user_id, role_id)" +
                "VALUES ((SELECT id FROM users WHERE login = ?), (SELECT id FROM roles WHERE naming = ?))";
        int row = 0;
        try (PreparedStatement add = DbConnector.get().prepareStatement(query);
             PreparedStatement add2 = DbConnector.get().prepareStatement(query2)){
            add.setString(1, user.getLogin());
            add.setString(2, user.getPassword());
            add.setString(3, user.getEmail());
            add.setString(4, user.getSurname());
            add.setString(5, user.getName());
            add.setString(6, user.getPatronymic());
            add.setDate(7, java.sql.Date.valueOf(user.getBirthday()));
            row = add.executeUpdate();

            add2.setString(1, user.getLogin());
            for (String role : user.getRoles()) {
                add2.setString(2, role);
                add2.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return row == 1;
    }
    public boolean editUser(User user) {
        String query = "UPDATE users SET " +
                "login = ?, password = ?, email = ?, surname = ?, name = ?, patronymic = ?, birthday = ? " +
                "WHERE login = ?";
        int row = 0;
        try (PreparedStatement edit = DbConnector.get().prepareStatement(query)) {
            edit.setString(1, user.getLogin());
            edit.setString(2, user.getPassword());
            edit.setString(3, user.getEmail());
            edit.setString(4, user.getSurname());
            edit.setString(5, user.getName());
            edit.setString(6, user.getPatronymic());
            edit.setDate(7, java.sql.Date.valueOf(user.getBirthday()));
            edit.setString(8, user.getLogin());
            row = edit.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return row == 1;
    }
    public boolean deleteUserWithLogin(String login) {
        String query1 = "DELETE FROM users_roles " +
                "WHERE user_id = (SELECT id FROM users WHERE login = ?)";
        String query2 = "DELETE FROM users WHERE login = ?";
        int row1 = 0, row2 = 0;
        try (PreparedStatement delete = DbConnector.get().prepareStatement(query1);
             PreparedStatement delete2= DbConnector.get().prepareStatement(query2)) {
            delete.setString(1, login);
            row1 = delete.executeUpdate();
            delete2.setString(1, login);
            row2 = delete2.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return row2 == 1;
    }

}
