package by.bar71.dao;

public class UserDaoSingleton {
    private static UserDaoSingleton userDaoSingleton = null;
    private UserDao userDao = null;
    private UserDaoSingleton() {
        userDao = UserDaoFactory.createDao("SQLDAO");
    }
    public static UserDao getValue() {
        if (userDaoSingleton == null) {
            userDaoSingleton = new UserDaoSingleton();
        }
        return userDaoSingleton.userDao;
    }
}
