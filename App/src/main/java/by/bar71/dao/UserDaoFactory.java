package by.bar71.dao;

public class UserDaoFactory {
    private enum Signs {SQLDAO, MEMORYDAO};
    public static UserDao createDao(String id) {
        Signs sign = Signs.valueOf(id.toUpperCase());
        UserDao userDao = null;
        switch (sign) {
            case SQLDAO: {
                userDao = new SqlUserDao();
                break;
            }
            case MEMORYDAO: {
                userDao = new InMemoryUserDao();
                break;
            }
        }
        return userDao;
    }
}
