package by.bar71.dao;

import by.bar71.domain.User;

import java.util.ArrayList;
import java.util.HashMap;

public interface UserDao {
    User findByLoginAndPassword(String login, String password);
    User findByLogin(String login);
    ArrayList<String> getUserRoles(String login);
    HashMap<String, User> getUsers();
    boolean addUser(User user);
    boolean editUser(User user);
    boolean deleteUserWithLogin(String login);
}
