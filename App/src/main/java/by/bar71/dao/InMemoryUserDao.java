package by.bar71.dao;

import by.bar71.data.InMemoryDataBase;
import by.bar71.domain.User;
import by.bar71.web.utils.ValidationLogic;

import java.util.ArrayList;
import java.util.HashMap;

public class InMemoryUserDao implements UserDao{
    public User findByLoginAndPassword(String login, String password){
        HashMap<String, User> users = InMemoryDataBase.getUserHashMap();
        User u = users.get(login);
        if (u != null && u.getPassword().equals(password)) {
            return u;
        }
        return null;
    }
    public User findByLogin(String login) {
        return InMemoryDataBase.getUserHashMap().get(login);
    }

    @Override
    public ArrayList<String> getUserRoles(String login) {
        return findByLogin(login).getRoles();
    }

    public HashMap<String, User> getUsers() {
        return InMemoryDataBase.getUserHashMap();
    }
    public boolean addUser(User user) {
        HashMap<String, User> users = InMemoryDataBase.getUserHashMap();
        if (!ValidationLogic.validateUser(user)) {
            return false;
        }
        users.put(user.getLogin(), user);
        return true;
    }
    public boolean editUser(User user) {
        return addUser(user);
    }
    public boolean deleteUserWithLogin(String login) {
        HashMap<String, User> users = InMemoryDataBase.getUserHashMap();
        if (!users.containsKey(login)) {
            return false;
        }
        users.remove(login);
        return true;
    }
}
