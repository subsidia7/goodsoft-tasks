package by.bar71.web.dispatchers;

import by.bar71.web.commands.Command;
import by.bar71.web.commands.ErrorCommand;
import by.bar71.web.commands.LoginCommand;
import by.bar71.web.commands.admincommands.AddUserAdminCommand;
import by.bar71.web.commands.admincommands.AdminCommand;
import by.bar71.web.commands.admincommands.DeleteUserAdminCommand;
import by.bar71.web.commands.admincommands.EditUserAdminCommand;
import by.bar71.web.commands.LogoutCommand;
import by.bar71.web.utils.ValidationLogic;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServiceDispatcher {
    private static HashMap<String, Command> services;

    public ServiceDispatcher() {
        services = new HashMap<String, Command>();
        services.put("login", new LoginCommand());
        services.put("logout", new LogoutCommand());
        services.put("addUser", new AddUserAdminCommand());
        services.put("editUser", new EditUserAdminCommand());
        services.put("deleteUser", new DeleteUserAdminCommand());
        services.put("noCommand", new ErrorCommand());
    }
    public String getPage(String command, HttpServletRequest req, HttpServletResponse res) {
        if (!services.containsKey(command)) {
            return services.get("noCommand").execute(req, res);
        }
        Command com = services.get(command);
        if (com instanceof AdminCommand admCom) {
            if (!ValidationLogic.visitorIsAdmin(req)) {
                return "/error";
            }
        }
        return com.execute(req, res);
    }
}
