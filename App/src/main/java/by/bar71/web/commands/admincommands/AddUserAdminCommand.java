package by.bar71.web.commands.admincommands;

import by.bar71.service.UserServiceSingleton;
import by.bar71.domain.User;
import by.bar71.web.utils.WebUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddUserAdminCommand implements AdminCommand {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse res) {
        User visitor = WebUtil.getUserFromRequest(req);
        if (UserServiceSingleton.getValue().findByLogin(visitor.getLogin()) == null &&
                UserServiceSingleton.getValue().addUser(visitor)) {
            req.getServletContext().setAttribute("dataBase", UserServiceSingleton.getValue().getUsers());
            return "/users";
        }
        return "/error";
    }
}
