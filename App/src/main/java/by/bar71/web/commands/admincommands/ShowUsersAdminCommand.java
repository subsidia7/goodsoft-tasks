package by.bar71.web.commands.admincommands;

import by.bar71.service.UserServiceSingleton;
import by.bar71.domain.User;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShowUsersAdminCommand implements AdminCommand {
    public String execute(HttpServletRequest req, HttpServletResponse res) {
        HashMap<String, User> users = UserServiceSingleton.getValue().getUsers();
        return "/users";
    }
}
