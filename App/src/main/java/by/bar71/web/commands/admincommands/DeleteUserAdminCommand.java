package by.bar71.web.commands.admincommands;

import by.bar71.service.UserServiceSingleton;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteUserAdminCommand implements AdminCommand {
    public String execute(HttpServletRequest req, HttpServletResponse res) {
        String visitorLogin = req.getParameter("visitorLogin");
        if (!UserServiceSingleton.getValue().deleteUserWithLogin(visitorLogin)) {
            return "/error";
        }
        req.getServletContext().setAttribute("dataBase", UserServiceSingleton.getValue().getUsers());
        return "/users";
    }
}
