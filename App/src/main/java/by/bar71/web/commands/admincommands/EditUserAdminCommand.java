package by.bar71.web.commands.admincommands;

import by.bar71.service.UserServiceSingleton;
import by.bar71.domain.User;
import by.bar71.web.utils.WebUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditUserAdminCommand implements AdminCommand {
    public String execute(HttpServletRequest req, HttpServletResponse res) {
        User user = WebUtil.getUserFromRequest(req);
        if (UserServiceSingleton.getValue().editUser(user)){
            req.getServletContext().setAttribute("dataBase", UserServiceSingleton.getValue().getUsers());
            return "/users";
        }
        return "/error";
    }
}
