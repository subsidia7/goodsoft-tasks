package by.bar71.web.commands;

import by.bar71.service.UserServiceSingleton;
import by.bar71.domain.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginCommand implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse res) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User visitor = UserServiceSingleton.getValue().findByLoginAndPassword(login, password);
        if (visitor == null) {
            return "/error";
        }
        addUserToSession(visitor, req);
        return "/welcome";
    }
    private void addUserToSession(User visitor, HttpServletRequest req) {
        HttpSession session = req.getSession(true);
        session.setAttribute("user", visitor);
    }
    public static boolean visitorIsLoggedIn(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return session != null && session.getAttribute("user") != null;
    }
}
