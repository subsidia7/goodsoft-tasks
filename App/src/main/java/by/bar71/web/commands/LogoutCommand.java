package by.bar71.web.commands;

import by.bar71.domain.User;
import by.bar71.web.utils.WebUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse res) {
        User u = WebUtil.getUserFromCurrentSession(req);
        if (u == null) {
            return "/error";
        }
        deleteUserFromSession(req);
        return "/login";
    }
    private void deleteUserFromSession(HttpServletRequest req) {
        HttpSession session = req.getSession(true);
        session.removeAttribute("user");
    }
}
