package by.bar71.web.utils;

import by.bar71.domain.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ValidationLogic {
    public static boolean validateUser(User user) {
        return user.getLogin() != null && user.getPassword() != null &&
                user.getEmail() != null && user.getRoles() != null &&
                user.getBirthday() != null && user.getSurname() != null &&
                user.getName() != null && user.getPatronymic() != null;
    }
    public static boolean visitorIsAdmin(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return session != null && session.getAttribute("user") != null
                && ((User) session.getAttribute("user")).getRoles().contains("admin");
    }
}
