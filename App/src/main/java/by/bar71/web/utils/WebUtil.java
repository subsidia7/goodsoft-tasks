package by.bar71.web.utils;

import by.bar71.domain.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class WebUtil {
    public static User getUserFromRequest(HttpServletRequest req) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        String admin = req.getParameter("admin");
        String tester = req.getParameter("tester");
        String manager = req.getParameter("manager");
        String director = req.getParameter("director");
        String security = req.getParameter("security");

        ArrayList<String> roles = new ArrayList<>();

        if (admin != null && admin.equals("on")) {
            roles.add("admin");
        }
        if (tester != null && tester.equals("on")) {
            roles.add("tester");
        }
        if (manager != null && manager.equals("on")) {
            roles.add("manager");
        }
        if (director != null && director.equals("on")) {
            roles.add("director");
        }
        if (security != null && security.equals("on")) {
            roles.add("security");
        }

        String email = req.getParameter("email");
        String surname = req.getParameter("surname");
        String name = req.getParameter("name");
        String patronymic = req.getParameter("patronymic");
        String birthday = req.getParameter("birthday");
        return new User(login, password, roles, email, surname, name, patronymic, birthday);
    }
    public static User getUserFromCurrentSession(HttpServletRequest req){
        HttpSession s = req.getSession(false);
        User u = null;
        if (s != null) {
            u = (User)s.getAttribute("user");
        }
        return u;
    }
}
