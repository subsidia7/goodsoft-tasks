package by.bar71.web.filters;

import by.bar71.web.commands.LoginCommand;

import javax.servlet.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {
    private FilterConfig filterConfig;
    public void init(final FilterConfig cfg) {
        filterConfig = cfg;
    }
    public void doFilter(final ServletRequest req, final ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException{
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        if (!LoginCommand.visitorIsLoggedIn(request)) {
            RequestDispatcher rd = req.getRequestDispatcher("/login");
            rd.forward(req, res);
        } else {
            filterChain.doFilter(req, res);
        }
    }
    public void destroy() {
    }
}
