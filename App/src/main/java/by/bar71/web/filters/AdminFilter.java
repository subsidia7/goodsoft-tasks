package by.bar71.web.filters;

import by.bar71.web.utils.ValidationLogic;

import javax.servlet.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {
    private FilterConfig filterConfig;
    public void init(final FilterConfig cfg) {
        filterConfig = cfg;
    }
    public void doFilter(final ServletRequest req, final ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException{
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        if (!ValidationLogic.visitorIsAdmin(request)) {
            String url = request.getContextPath() + "/error";
            response.sendRedirect(url);
        } else {
            filterChain.doFilter(req, res);
        }
    }
    public void destroy() {
    }
}
