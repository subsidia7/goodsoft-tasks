package by.bar71.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import by.bar71.service.UserServiceSingleton;
import by.bar71.web.dispatchers.ServiceDispatcher;

@WebServlet("/controller")
public class Controller extends HttpServlet {
    private ServiceDispatcher serviceDispatcher;
    @Override
    public void init() throws ServletException {
        super.init();
        getServletContext().setAttribute("dataBase", UserServiceSingleton.getValue().getUsers());
        serviceDispatcher = new ServiceDispatcher();
    }
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        handleRequest(req, res);
    }
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        handleRequest(req, res);
    }
    public void handleRequest(HttpServletRequest req, HttpServletResponse res)  {
        try {
            PrintWriter out = res.getWriter();
            String command = req.getParameter("command");
            //String email = req.getParameter("email");
            String page = serviceDispatcher.getPage(command, req, res);
            String url = req.getContextPath() + page;
            res.sendRedirect(url);
        } catch (Exception e) {
            log(e.getMessage());
        }
    }
}
