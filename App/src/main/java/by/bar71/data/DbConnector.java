package by.bar71.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
    private static Connection connection = null;
    public static Connection get() {
        if (connection == null) {
            String url = "jdbc:postgresql://localhost:5432/auth";
            String login = "postgres";
            String password = "root";
            try {
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(url, login, password);
            } catch (ClassNotFoundException | SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return connection;
    }
}
