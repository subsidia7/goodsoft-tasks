package by.bar71.data;

import by.bar71.domain.User;

import java.util.HashMap;

public class InMemoryDataBase {
    private static HashMap<String, User> userHashMap = null;
    public static HashMap<String, User> getUserHashMap() {
        if (userHashMap == null) {
            fillArrayWithData();
        }
        return userHashMap;
    }
    private static void fillArrayWithData() {
        userHashMap = new HashMap<>();
//        userHashMap.put("admin1", new User("admin1", "admin1", "admin", "admin1@gmail.com", "adminov1", "admin1", "adminovich1", "01.01.1961"));
//        userHashMap.put("admin2", new User("admin2", "admin2", "admin", "admin2@gmail.com", "adminov2", "admin2", "adminovich2", "02.02.1962"));
//        userHashMap.put("dima", new User("dima", "qwe", "user", "dima@gmail.com", "Kheruvimov", "Dmitriy", "Konstantinovich", "29.05.2001"));
//        userHashMap.put("bob", new User("bob", "123", "user", "user@mail.ru", "Darel", "Bob", "Bobik", "02.12.2010"));
//        userHashMap.put("helena", new User("helena", "kindgirl", "user", "helena@gmail.com", "Kotova", "Helena", "Urievna", "11.03.2005"));
//        userHashMap.put("john", new User("john", "123987", "user", "john@gmail.com", "Willow", "John", "Lobsterovich", "05.10.2002"));
//        userHashMap.put("steklorezka20", new User("steklorezka20", "donetsk12", "user", "steklorezka20@gmail.com", "Sidorenko", "Anastasia", "Andreevna", "01.12.2003"));
//
    }
}
