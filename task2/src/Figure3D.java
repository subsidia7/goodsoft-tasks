import java.math.BigDecimal;

public abstract class Figure3D implements Printable{
    protected abstract BigDecimal capacity();
    public void print() {
        System.out.println("This is TELO, capacity = " + capacity());
    }
}
