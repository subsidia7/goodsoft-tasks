import java.math.BigDecimal;
import java.math.RoundingMode;

public class Triangle extends Figure2D {
    protected BigDecimal ab;
    protected BigDecimal bc;
    protected BigDecimal ac;
    Triangle(BigDecimal ab, BigDecimal bc, BigDecimal ac) {
        this.ab = ab;
        this.bc = bc;
        this.ac = ac;
        name = "Triangle";
    }
    @Override
    protected BigDecimal perimeter() {
        return ab.add(bc).add(ac).setScale(3, RoundingMode.HALF_UP);
    }

}
