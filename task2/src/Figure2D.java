import java.math.BigDecimal;

public abstract class Figure2D implements Printable{
    protected String name;
    protected abstract BigDecimal perimeter();
    public void print() {
        System.out.println("This is FIGURA " + name + " , perimeter = " + perimeter());
    }
}
