import java.math.BigDecimal;
import java.math.RoundingMode;

public class Rectangle extends Figure2D {
    protected BigDecimal ab;
    protected BigDecimal bc;
    Rectangle(BigDecimal ab, BigDecimal bc) {
        this.ab = ab;
        this.bc = bc;
        name = "Rectangle";
    }
    @Override
    protected BigDecimal perimeter() {
        return (ab.add(bc).multiply(BigDecimal.valueOf(2).setScale(3, RoundingMode.HALF_UP)));
    }
}
