import java.math.BigDecimal;
import java.math.RoundingMode;

public class Trapeze extends Figure2D {
    protected BigDecimal ab;
    protected BigDecimal bc;
    protected BigDecimal cd;
    protected BigDecimal ad;
    Trapeze(BigDecimal ab, BigDecimal bc, BigDecimal cd, BigDecimal ad) {
        this.ab = ab;
        this.bc = bc;
        this.cd = cd;
        this.ad = ad;
        name = "Trapeze";
    }
    @Override
    protected BigDecimal perimeter() {
        return ab.add(bc).add(cd).add(ad).setScale(3, RoundingMode.HALF_UP);
    }
}
