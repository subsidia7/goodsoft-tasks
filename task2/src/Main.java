import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Figure2D t = new Triangle(BigDecimal.valueOf(1),
                                  BigDecimal.valueOf(2),
                                  BigDecimal.valueOf(3));
        Figure3D p = new Cube(BigDecimal.valueOf(0.333333333333333));
        t.print();
        p.print();
    }
}
