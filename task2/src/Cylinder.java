import java.math.BigDecimal;
import java.math.RoundingMode;

public class Cylinder extends Figure3D {
    protected BigDecimal r;
    protected BigDecimal h;
    Cylinder(BigDecimal r, BigDecimal h) {
        this.r = r;
        this.h = h;
    }
    protected BigDecimal capacity() {
        return r.multiply(h).setScale(3, RoundingMode.HALF_UP);
    }
}
