import java.math.BigDecimal;
import java.math.RoundingMode;

public class Cone extends Figure3D {
    protected BigDecimal r;
    protected BigDecimal h;
    Cone(BigDecimal r, BigDecimal h) {
        this.r = r;
        this.h = h;
    }
    public BigDecimal capacity() {
        return r.multiply(h).divide(BigDecimal.valueOf(3).setScale(3, RoundingMode.HALF_UP));
    }

}
