import java.math.BigDecimal;
import java.math.RoundingMode;

public class Cube extends Figure3D {
    protected BigDecimal a;
    Cube(BigDecimal a) {
        this.a = a;
    }
    public BigDecimal capacity() {
        return a.multiply(a).multiply(a).setScale(3, RoundingMode.HALF_UP);
    }
}
